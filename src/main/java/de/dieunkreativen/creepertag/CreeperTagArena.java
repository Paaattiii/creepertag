/*
* CreeperTag is a plugin based off of the popular minigame TNTTag.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.creepertag;

import java.util.ArrayList;
import java.util.Collection;

import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.events.ArenaEventHandler;

import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;

public class CreeperTagArena extends Arena{
	@SuppressWarnings("deprecation")
	ItemStack is = new ItemStack(Material.getMaterial(397), 1, (short) 4);
	int task1, task2, task3;
	
	
	public void onStart(){
		setRandomCreeper();
	}
	
	public void setRandomCreeper(){
		int randomint = (int) Math.abs(Math.random()*getArenaPlayer().size());
		int i = 0;
		for (ArenaPlayer ap: getArenaPlayer()){
			ap.getPlayer().getInventory().clear();
			if (i == randomint && getArenaPlayer().size() > 1) {
				ap.getPlayer().getInventory().setHelmet(is);
				fireworkEffekt(ap.getPlayer().getLocation());
				timer();
				for(int z = 0; z < 9; z++) {
					ap.getPlayer().getInventory().setItem(z, is);
				}
			}
			
			i++;
		}
	}
	
	public void changeCreeper(Player damager, Player player) {
		player.getInventory().setHelmet(is);
		damager.getInventory().clear();
		damager.getInventory().setHelmet(null);
		
		for(int z = 0; z < 9; z++) {
			player.getPlayer().getInventory().setItem(z, is);
		}
			
		fireworkEffekt(player.getLocation());
			
	}
	
	public void fireworkEffekt(Location loc) {
		Location location = new Location(loc.getWorld(), loc.getX(), loc.getY() + 3, loc.getZ());
		final Firework firework = location.getWorld().spawn(location, Firework.class);
		FireworkMeta data = (FireworkMeta) firework.getFireworkMeta();
		data.addEffects(FireworkEffect.builder().trail(true).flicker(true).withColor(Color.LIME).with(Type.CREEPER).build());
		data.setPower(0);
		firework.setFireworkMeta(data);
			
		task1 = Bukkit.getScheduler().scheduleSyncDelayedTask(CreeperTag.getSelf(), new Runnable() {
			public void run() {
				firework.detonate();
				Bukkit.getScheduler().cancelTask(task1);
				}
			}, 3L); 
	}
	
	public void timer() {
		task2 = Bukkit.getScheduler().scheduleSyncRepeatingTask(CreeperTag.getSelf(), new Runnable() {
			int timer = 30;
			public void run()
            {
                for (ArenaPlayer p : getArenaPlayer())
                    p.getPlayer().setLevel(timer - 1); // decrease all levels by 1


                timer--;
                if (timer == 0)
                {
                    for (ArenaPlayer ap : getArenaPlayer())
                        if (ap.getPlayer().getInventory().getHelmet() != null) {
                        	PlayerDeathEvent ede = new PlayerDeathEvent(ap.getPlayer(),new ArrayList<ItemStack>(),0, "");
            	        	Bukkit.getPluginManager().callEvent(ede);
                        	Location loc = ap.getLocation();
                        	Location location = new Location(loc.getWorld(), loc.getX(), loc.getY() + 1, loc.getZ());
                        	location.getWorld().createExplosion(location, 0.0F);
                        	Bukkit.getScheduler().cancelTask(task2);
                        	
                        	task3 = Bukkit.getScheduler().scheduleSyncDelayedTask(CreeperTag.getSelf(), new Runnable() {
                    			public void run() {
                    				setRandomCreeper();
                    				Bukkit.getScheduler().cancelTask(task3);
                    				}
                    			}, 60L); 
                        }
                }
            }
			}, 0L, 20L);
		
		
		
	}
	
	
	public Collection<ArenaPlayer> getArenaPlayer() {
		return getMatch().getAlivePlayers();
	}
	
	
	@ArenaEventHandler(suppressCastWarnings=true)
	 public void onDamage(EntityDamageByEntityEvent event){
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			Player damager = (Player) event.getDamager(); //Spieler der einen anderen geschlagen hat
			Player player = (Player) event.getEntity(); //Wurde von einem Spieler geschlagen
			
			if(damager.getInventory().getHelmet() != null) {
				changeCreeper(damager, player);
			}
		}
	}
	
	@ArenaEventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		event.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	@ArenaEventHandler
	public void onEntityDamage(EntityDamageEvent Event) {
			Event.setDamage(0);
		}
	
	@ArenaEventHandler
	public void onPlayerDeath(PlayerDeathEvent Event) {
			Event.getDrops().clear();
	}
	
	@ArenaEventHandler
	public void onBlockPlace(BlockPlaceEvent Event) {	//Bl�cke d�rfen w�hrend des Spiels nicht platziert werden
		Event.setCancelled(true);
    }
	
	@ArenaEventHandler
	public void onBlockBreak(BlockBreakEvent Event) {	//Bl�cke d�rfen w�hrend des Spiels nicht abgebaut werden
		Event.setCancelled(true);
	}
	
	@ArenaEventHandler
	public void onPlayerDrop(PlayerDropItemEvent Event) {
		Event.setCancelled(true);
  }
   
}
