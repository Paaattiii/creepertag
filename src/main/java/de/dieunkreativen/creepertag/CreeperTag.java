/*
* CreeperTag is a plugin based off of the popular minigame TNTTag.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.creepertag;

import mc.alk.arena.BattleArena;

import org.bukkit.plugin.java.JavaPlugin;

public class CreeperTag extends JavaPlugin {
	static CreeperTag plugin;
	
	
	@Override
	public void onEnable() {
		plugin = this;
		System.out.println("[Paintball] Erfolgreich geladen!");
		BattleArena.registerCompetition(this, "CreeperTag", "ct", CreeperTagArena.class);

	}
	@Override
	public void onDisable() {
		System.out.println("[Paintball] Erfolgreich deaktiviert!");
	}
	
	public static CreeperTag getSelf() {
		return plugin;
	}
	
	
	
}
	
